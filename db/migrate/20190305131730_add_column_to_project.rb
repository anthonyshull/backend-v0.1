# frozen_string_literal: true

class AddColumnToProject < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :status, :integer
    add_column :projects, :short_name, :string
    add_column :projects, :is_private, :boolean
  end
end

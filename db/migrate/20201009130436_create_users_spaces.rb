class CreateUsersSpaces < ActiveRecord::Migration[5.2]
  def change
    create_table :users_spaces do |t|
      t.belongs_to :user, index: true
      t.belongs_to :space, index: true
      t.string :role
      t.string :part
    end
  end
end

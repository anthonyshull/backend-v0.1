# frozen_string_literal: true

class AddUserCol < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :affiliation, :string
    add_column :users, :category, :string
  end
end

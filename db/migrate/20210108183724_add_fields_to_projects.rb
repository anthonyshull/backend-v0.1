class AddFieldsToProjects < ActiveRecord::Migration[6.1]
  def change
    add_column :projects, :is_looking_for_collaborators, :boolean, default: false
    add_column :projects, :maturity, :integer
  end
end

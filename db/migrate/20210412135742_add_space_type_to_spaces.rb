class AddSpaceTypeToSpaces < ActiveRecord::Migration[6.1]
  def change
    add_column :spaces, :space_type, :integer, default: 0
  end
end

# frozen_string_literal: true

class CreateEnablers < ActiveRecord::Migration[5.2]
  def change
    create_table :enablers do |t|
      t.string :description
      t.string :name
      t.string :organisation_type
      t.timestamps
    end
  end
end

# frozen_string_literal: true

class RemoveLogoUrlColumn < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :logo_url
  end
end

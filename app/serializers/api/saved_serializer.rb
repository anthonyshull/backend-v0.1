# frozen_string_literal: true

class Api::SavedSerializer < ActiveModel::Serializer
  # THIS IS THE WAY

  has_many :projects do
    object.owned_relations.saved.of_type('Project').includes(:resource).map(&:resource).compact
  end

  has_many :users do
    object.owned_relations.saved.of_type('User').includes(:resource).map(&:resource).compact
  end

  has_many :needs do
    object.owned_relations.saved.of_type('Need').includes(:resource).map(&:resource).compact
  end

  has_many :challenges do
    object.owned_relations.saved.of_type('Challenge').includes(:resource).map(&:resource).compact
  end

  has_many :communities do
    object.owned_relations.saved.of_type('Community').includes(:resource).map(&:resource).compact
  end

  has_many :programs do
    object.owned_relations.saved.of_type('Program').includes(:resource).map(&:resource).compact
  end
end

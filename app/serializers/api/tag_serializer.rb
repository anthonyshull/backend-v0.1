# frozen_string_literal: true

class Api::TagSerializer < ActiveModel::Serializer
  attributes :name
end

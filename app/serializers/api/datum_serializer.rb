# frozen_string_literal: true

class Api::DatumSerializer < ActiveModel::Serializer
  attributes :id,
             :title,
             :description,
             :url,
             :format,
             :filename,
             :provider_id,
             :provider_created_at,
             :provider_updated_at,
             :provider_version

  has_many :datafields
end

# frozen_string_literal: true

class Tag < ApplicationRecord
  belongs_to :tagable, polymorphic: true
  include RecsysHelpers

  # Attributes
  # :name
end

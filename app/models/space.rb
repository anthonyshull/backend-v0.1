# frozen_string_literal: true

# Umbrella object that holds this hierarchy: programs -> challenges -> projects -> needs
# Users can become associated to the space

class Space < ApplicationRecord
  resourcify
  notification_object
  include AlgoliaSearch
  include AttrJson::Record

  include AffiliatableParent
  include Avatarable
  include Bannerable
  include Boardable
  include Feedable
  include Linkable
  include Membership
  include NotificationsHelpers
  include RecsysHelpers
  include RelationHelpers
  include Utils

  valid_affiliates :challenge, :program, :project, :user

  attr_json :available_tabs, :json, default:
    {
      programs: true,
      challenges: true,
      resources: true,
      enablers: true,
      faqs: true
    }

  attr_json :selected_tabs, :json, default:
    {
      programs: true,
      challenges: true,
      resources: true,
      enablers: true,
      faqs: true
    }
  attr_json :show_featured, :json, default: {
    challenges: true,
    needs: true,
    programs: true,
    projects: true
  }

  has_many :programs
  has_many :resources, as: :documentable, class_name: 'Document', dependent: :destroy

  has_one :faq, as: :faqable, dependent: :destroy
  belongs_to :creator, foreign_key: 'creator_id', class_name: 'User'

  has_many_attached :documents

  # TODO: FIX THE VALIDATION FOR HAS_MANY

  # validates :documents, content_type: ["image/png",
  #                                      "image/jpeg",
  #                                      "image/gif",
  #                                      "text/csv",
  #                                      "application/pdf",
  #                                      "image/svg+xml",
  #                                      "application/x-tar",
  #                                      "image/tiff",
  #                                      "application/zip",
  #                                      "application/x-7z-compressed"
  #                                    ]
  # validates :documents, content_size: 31457280  # Document must be less than 30Mb

  validates :short_title, uniqueness: true
  validates :title, presence: true

  # opportunity: state machine
  # adds methods: draft? soon? active? completed?
  enum status: %i[draft soon active completed archived]
  enum space_type: {  digital_community: 0,
                      local_community: 1,
                      ngo: 2,
                      not_for_profit_organization: 3,
                      startup: 4,
                      maker_space: 5,
                      community_lab: 6,
                      company: 7,
                      social_enterprise: 8,
                      school: 9,
                      foundation: 10,
                      academic_lab: 11,
                      professional_network: 12,
                      public_agency: 13,
                      public_institution: 14,
                      incubator: 15,
                      living_lab: 16,
                      fund: 17,
                      other: 18
                    }

  before_create :sanitize_description
  # after_save :create_notifications
  before_update :sanitize_description

  # don't index draft spaces to algolia, in production env
  algoliasearch disable_indexing: !Rails.env.production?, unless: :draft? do
    use_serializer Api::SpaceSerializer
  end

  # def add_user_from_programs
  #   programs.each do |program|
  #     program.users.each do |user|
  #       next if users.include? user

  #       users << user
  #       user.add_role :member, self unless user.has_role? :member, self
  #     end
  #   end
  # end

  def needs_count
    programs.includes(challenges: { challenges_projects: { project: :needs } }).where("challenges_projects.project_status = #{ChallengesProject.project_statuses[:accepted]}").count(:needs)
  end

  def projects_count
    programs.includes(challenges: { challenges_projects: :project }).where("challenges_projects.project_status = #{ChallengesProject.project_statuses[:accepted]}").count(:projects)
  end

  def frontend_link
    "/space/#{short_title}"
  end

  # We only want to send notifications during certain status transitions
  # opportunity: state machine
  # def create_notifications
  #   return unless saved_change_to_status?

  #   if status_before_last_save == 'draft' && soon?
  #     notif_soon_space
  #   elsif status_before_last_save == 'soon' && active?
  #     notif_start_space
  #   elsif status_before_last_save == 'active' && completed?
  #     notif_end_space
  #   end
  # end

  # def notif_soon_space
  #   Notification.for_group(
  #     :everyone,
  #     attrs: {
  #       category: :space,
  #       type: 'soon_space',
  #       object: self,
  #       metadata: {
  #         space_id: id
  #       }
  #     }
  #   )
  # end

  # def notif_start_space
  #   Notification.for_group(
  #     :everyone,
  #     attrs: {
  #       category: :space,
  #       type: 'start_space',
  #       object: self,
  #       metadata: {
  #         space_id: id
  #       }
  #     }
  #   )
  # end

  # def notif_end_space
  #   Notification.for_group(
  #     :everyone,
  #     attrs: {
  #       category: :space,
  #       type: 'end_space',
  #       object: self,
  #       metadata: {
  #         space_id: id
  #       }
  #     }
  #   )
  # end

  # send notif to user who requests to join private space
  def notif_pending_join_request(author)
    Notification.create(
      target: author,
      category: :membership,
      type: 'pending_join_request',
      object: self,
      metadata: { author_id: author.id }
    )
  end

  # send notification/email when user is approved to join private object
  def notif_pending_join_request_approved(member)
    Notification.create(
      target: member,
      category: :membership,
      type: 'pending_join_request_approved',
      object: self
    )
  end

  def all_owners_admins_members
    query = owners
            .or(admins)
            .or(members)
            .distinct
            .active
    programs.each do |program|
      query = query.or(program.all_owners_admins_members)
    end
    query
  end

  # def owners
  #   User.with_role(:owner, self)
  # end

  # def admins
  #   User.with_role(:admin, self)
  # end

  # def members
  #   User.with_role(:member, self)
  # end

  private

  def default_banner_url
    ActionController::Base.helpers.image_url('default-program.jpg')
  end

  def default_avatar_url
    ActionController::Base.helpers.image_url('default-avatar.png')
  end
end

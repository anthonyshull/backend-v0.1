# frozen_string_literal: true

class Project < ApplicationRecord
  has_merit

  # Posts must have a unique short name.
  resourcify
  notification_object
  include AlgoliaSearch
  include RelationHelpers
  include Coordinates
  include Utils
  include RecsysHelpers
  include NotificationsHelpers
  include Bannerable
  include Linkable
  include Feedable
  include Membership
  include Geocodable
  include Skillable
  include Interestable

  has_many :challenges_projects
  has_many :challenges, through: :challenges_projects, after_add: %i[update_activity reindex], after_remove: %i[update_activity reindex]
  has_many :needs_projects
  has_many :needs, through: :needs_projects, after_add: %i[update_activity], after_remove: %i[update_activity]
  belongs_to :creator, foreign_key: 'creator_id', class_name: 'User'

  has_many_attached :documents

  has_many :customfields, as: :resource

  has_many :externalhooks, as: :hookable, after_add: %i[update_activity], after_remove: %i[update_activity]

  validates :short_title, uniqueness: true
  validates :title, presence: true, uniqueness: true

  before_create :create_coordinates
  # before_create :create_avatars

  before_create :sanitize_description
  before_update :sanitize_description

  enum status: %i[active archived draft completed on_hold terminated]
  enum maturity: %i[problem_statement solution_proposal prototype proof_of_concept implementation demonstrated_impact]

  # After the model is validated geocode !
  # after_validation :geocode, on: [ :update ]

  geocoded_by :make_address
  after_validation :geocode

  # don't index draft projects to algolia, in production env
  algoliasearch disable_indexing: !Rails.env.production?, unless: :draft? do
    use_serializer Api::ProjectSerializer
    geoloc :latitude, :longitude
  end

  def needs_count
    needs.count
  end

  def update_activity(_obj)
    self.updated_at = Time.now
  end

  def frontend_link
    "/project/#{id}"
  end

  def notif_pending_join_request(author)
    Notification.create(
      target: author,
      category: :membership,
      type: 'pending_join_request',
      object: self,
      metadata: { author_id: author.id }
    )
  end

  def notif_pending_join_request_approved(member)
    Notification.create(
      target: member,
      category: :membership,
      type: 'pending_join_request_approved',
      object: self
    )
  end

  private

  def default_banner_url
    ActionController::Base.helpers.image_url('default-project.jpg')
  end
end

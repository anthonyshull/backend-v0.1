# frozen_string_literal: true

class Interest < ApplicationRecord
  include RecsysHelpers

  has_and_belongs_to_many :users
  has_and_belongs_to_many :projects
  has_and_belongs_to_many :community
  has_and_belongs_to_many :challenge
end

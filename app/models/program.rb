# frozen_string_literal: true

# Umbrella object that holds this heirarchy: challenges -> projects -> needs
# Users can join the program (initiative)
# Can have a start and end date (as yet unused)

class Program < ApplicationRecord
  resourcify
  notification_object
  include AlgoliaSearch

  include AffiliatableChild
  include RelationHelpers
  include Utils
  include RecsysHelpers
  include NotificationsHelpers
  include Avatarable
  include Bannerable
  include Linkable
  include Boardable
  include Feedable
  include Membership

  belongs_to :space, optional: true

  has_many :challenges
  has_many :resources, as: :documentable, class_name: 'Document', dependent: :destroy

  has_one :faq, as: :faqable, dependent: :destroy

  validates :short_title, uniqueness: true
  validates :title, presence: true

  # opportunity: state machine
  # adds methods: draft? soon? active? completed?
  enum status: %i[draft soon active completed]

  before_create :sanitize_description
  # after_save :create_notifications
  before_update :sanitize_description

  # don't index draft programs to algolia, in production env
  algoliasearch disable_indexing: !Rails.env.production?, unless: :draft? do
    use_serializer Api::ProgramSerializer
  end

  def add_user_from_challenges
    challenges.each do |challenge|
      challenge.users.each do |user|
        next if users.include? user

        user.add_role(:member, self)
      end
    end
  end

  def needs_count
    challenges.includes(challenges_projects: { project: :needs }).where("challenges_projects.project_status = #{ChallengesProject.project_statuses[:accepted]}").count(:needs)
  end

  def projects_count
    challenges.includes(:projects).where("challenges_projects.project_status = #{ChallengesProject.project_statuses[:accepted]}").count(:projects)
  end

  # def notif_end_challenge(challenge)
  #   Notification.for_group(
  #     :members,
  #     args: [self],
  #     attrs: {
  #       category: :program,
  #       type: 'end_challenge',
  #       object: challenge,
  #       metadata: {
  #         program_id: id
  #       }
  #     }
  #   )
  # end

  # # We only want to send notifications during certain status transitions
  # # opportunity: state machine
  # def create_notifications
  #   return unless saved_change_to_status?

  #   if status_before_last_save == 'draft' && soon?
  #     notif_soon_program
  #   elsif status_before_last_save == 'soon' && active?
  #     notif_start_program
  #   elsif status_before_last_save == 'active' && completed?
  #     notif_end_program
  #   end
  # end

  # def notif_soon_program
  #   Notification.for_group(
  #     :everyone,
  #     attrs: {
  #       category: :program,
  #       type: 'soon_program',
  #       object: self,
  #       metadata: {
  #         program_id: id
  #       }
  #     }
  #   )
  # end

  # def notif_start_program
  #   Notification.for_group(
  #     :everyone,
  #     attrs: {
  #       category: :program,
  #       type: 'start_program',
  #       object: self,
  #       metadata: {
  #         program_id: id
  #       }
  #     }
  #   )
  # end

  # def notif_end_program
  #   Notification.for_group(
  #     :everyone,
  #     attrs: {
  #       category: :program,
  #       type: 'end_program',
  #       object: self,
  #       metadata: {
  #         program_id: id
  #       }
  #     }
  #   )
  # end

  # NOTE: the frontend has chosen "program" singular vs "programs" plural
  def frontend_link
    "/program/#{short_title}"
  end

  def all_owners_admins_members
    query = super()
    challenges.each do |challenge|
      query = query.or(challenge.all_owners_admins_members)
    end
    query
  end

  def user_is_member?(user)
    return true if super(user)

    challenges.any? { |challenge| challenge.user_is_member?(user) }
  end

  private

  def default_banner_url
    ActionController::Base.helpers.image_url('default-program.jpg')
  end

  def default_avatar_url
    ActionController::Base.helpers.image_url('default-avatar.png')
  end
end

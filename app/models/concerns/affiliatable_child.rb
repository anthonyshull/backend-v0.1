# frozen_string_literal: true

module AffiliatableChild
  extend ActiveSupport::Concern

  included do
    has_many :affiliations, as: :affiliate

    def self.valid_affiliations(*valid_affiliation_types)
      @@valid_affiliation_types = valid_affiliation_types
    end
  end

  def add_affiliation_to(parent)
    if @@valid_affiliation_types.include?(affiliate)
      ::Affiliation.create(parent: parent, affiliate: self)
    else
      raise "Invalid affiliation type :#{affiliate.class.to_s.downcase}, must be one of #{@@valid_affiliation_types.inspect}"
    end
  end

  def remove_affiliation_to(parent)
    ::Affiliation.delete(parent: parent, affiliate: self)
  end
end

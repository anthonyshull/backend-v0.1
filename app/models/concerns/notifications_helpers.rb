# frozen_string_literal: true

module NotificationsHelpers
  extend ActiveSupport::Concern

  def notif_new_member(author)
    Notification.for_group(
      :admins,
      args: [self],
      attrs: {
        category: :membership,
        type: 'new_member',
        object: self,
        metadata: {
          author_id: author.id
        }
      }
    )
  end

  def notif_new_follower(author)
    Notification.for_group(
      :admins,
      args: [self],
      attrs: {
        category: :follow,
        type: 'new_follower',
        object: self,
        metadata: {
          author_id: author.id
        }
      }
    )
  end

  def notif_new_clap(author)
    Notification.for_group(
      :admins,
      args: [self],
      attrs: {
        category: :clap,
        type: 'new_clap',
        object: self,
        metadata: {
          author_id: author.id
        }
      }
    )
  end

  def notif_pending_member(author)
    return unless self.has_privacy?
    return unless is_private

    Notification.for_group(
      :admins,
      args: [self],
      attrs: {
        category: :administration,
        type: 'pending_member',
        object: self,
        metadata: {
          from_type: self.class.name,
          from_id: id,
          author_id: author.id
        }
      }
    )
  end
end

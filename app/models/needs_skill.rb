# frozen_string_literal: true

class NeedsSkill < ApplicationRecord
  belongs_to :need
  belongs_to :skill
end

# frozen_string_literal: true

class Feed < ApplicationRecord
  resourcify
  belongs_to :feedable, polymorphic: true
  has_and_belongs_to_many :posts, after_add: %i[update_activity]

  def notif_new_post_in_feed(post, feed_host)
    Notification.for_group(
      :feed_host_followers_minus_post_author,
      args: [post, feed_host],
      attrs: {
        category: :feed,
        type: 'new_post',
        object: post,
        metadata: {
          author_id: post.user.id
        }
      }
    )
  end

  def notify_admins_about_new_post(post)
    Notification.for_group(
      :admins_minus_poster,
      args: [feedable, post],
      attrs: {
        category: :feed,
        type: 'new_post_admin_notify',
        object: post,
        metadata: {
          author_id: post.user.id
        }
      }
    )
  end

  def update_activity(_args)
    feedable.updated_at = Time.now
  end
end

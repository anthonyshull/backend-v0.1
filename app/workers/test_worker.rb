# frozen_string_literal: true

class TestWorker
  include Sidekiq::Worker

  def perform
    puts "POSITIVE ATTITUDE!"
  end
end

# frozen_string_literal: true

module Api::Upload
  extend ActiveSupport::Concern
  include Rails.application.routes.url_helpers

  included do
    before_action :upload_authenticate_user!, only: %i[upload_avatar upload_document upload_banner remove_document remove_avatar remove_banner]
  end

  def upload_avatar
    raise ApiExceptions::UnprocessableEntity.new(message: 'Missing avatar') if params[:avatar].nil?
    raise ApiExceptions::UnprocessableEntity.new(message: 'Invalid image type') unless authorized_image_type? params[:avatar]

    @obj.avatar.attach(params[:avatar])

    raise ApiExceptions::UnprocessableEntity.new(message: 'Something went wrong') unless @obj.avatar.attached?

    @obj.avatar.filename = @obj.avatar.filename.to_s.sub('(', '-').sub(')', '-')
    @obj.avatar.save!
    variant = @obj.avatar.variant(resize: '200x200^')
    logo_url = rails_representation_url(variant)
    render json: { data: 'Avatar uploaded', url: logo_url }, status: :ok
    @obj.save!
  end

  def upload_banner
    raise ApiExceptions::UnprocessableEntity.new(message: 'Missing banner') if params[:banner].nil?
    raise ApiExceptions::UnprocessableEntity.new(message: 'Invalid image type') unless authorized_image_type? params[:banner]

    @obj.banner.attach(params[:banner])

    raise ApiExceptions::UnprocessableEntity.new(message: 'Something went wrong') unless @obj.banner.attached?

    @obj.banner.filename = @obj.banner.filename.to_s.sub('(', '-').sub(')', '-')
    @obj.banner.save!
    render json: { data: 'Banner uploaded', url: Rails.application.routes.url_helpers.rails_blob_url(@obj.banner) }, status: :ok
    @obj.save!
  end

  def upload_document
    documents = params[:documents]
    raise ApiExceptions::UnprocessableEntity.new(message: 'No documents') if documents.nil?

    documents.each do |document|
      @obj.documents.attach(document)
    end

    raise ApiExceptions::UnprocessableEntity.new(message: 'Something went wrong') unless @obj.documents.attached?

    render json: { data: 'documents uploaded' }, status: :ok
  end

  def remove_document
    doc_id = params[:document_id]

    raise ApiExceptions::UnprocessableEntity.new(message: 'Missing document id') if doc_id.nil?
    raise ApiExceptions::ResourceNotFound.new(message: 'Object has no documents!') unless @obj.documents.attached?

    found = false
    @obj.documents.each do |document|
      if document.blob.id == doc_id.to_i
        found = true
        document.purge
      end
    end

    raise ApiExceptions::ResourceNotFound.new(message: 'Cannot find document!') unless found

    render json: { data: 'Document removed' }, status: :ok
  end

  def remove_avatar
    raise ApiExceptions::ResourceNotFound.new(message: 'No avatar attached') unless @obj.avatar.attached?

    @obj.avatar.purge
    render json: { data: 'Avatar removed' }, status: :ok
  end

  def remove_banner
    raise ApiExceptions::ResourceNotFound.new(message: 'No banner attached') unless @obj.banner.attached?

    @obj.banner.purge
    render json: { data: 'Banner removed' }, status: :ok
  end

  private

  def upload_authenticate_user!
    authenticate_user!
  end

  def authorized_image_type?(uploaded_io)
    image = MiniMagick::Image.new(uploaded_io.tempfile.path)
    %w[GIF JPEG PNG].include?(image.type)
  end
end

# frozen_string_literal: true

module Api::Members
  extend ActiveSupport::Concern

  included do
    before_action :member_authenticate_user!, only: %i[leave join update_member remove_member invite]
    before_action :can_remove?, only: [:remove_member]
  end

  # With a given user id returns find or not found if the given user is a member
  # of the given object

  def has_membership
    @user = User.find(params[:user_id])
    if @obj.users.include?(@user)
      render json: { data: 'The current user is object member' }, status: :found
    else
      render json: { data: 'The current user is not a object member' }, status: :not_found
    end
  end

  # Same logic is applied here, the given user is deleted from the relation of
  # the given object
  def remove_member
    @user = User.find(params[:user_id])

    render(json: { data: 'You cannot have zero owner' }, status: :forbidden) && return if (User.with_role(:owner, @obj).count == 1) && @user.has_role?(:owner, @obj)

    @obj.applied_roles.pluck(:name).each do |role_name|
      @user.remove_role(role_name, @obj)
      @user.remove_edge(@obj, "is_#{role_name}_of")
    end

    render json: { data: 'The user was removed' }, status: :found
  end

  # Same logic is applied here, with given object and given user we verify user role
  # for that object, if he has the rights on the given object he can update his role
  def update_member
    @user = User.find(params[:user_id])
    if (new_role = params[:new_role])
      previous_role = get_current_role(@user, @obj)
      if (new_role == 'owner') && !current_user.has_role?(:owner, @obj)
        render(json: { data: 'Only an owner can set an owner' }, status: :forbidden) && return

      elsif (new_role == 'admin') && !current_user.has_role?(:admin, @obj)
        render(json: { data: 'Only an owner or admin can set an admin' }, status: :forbidden) && return

      elsif (previous_role == 'owner') && !current_user.has_role?(:owner, @obj)
        render(json: { data: "You cannot demote an owner if you aren't one" }, status: :forbidden) && return

      elsif (User.with_role(:owner, @obj).count == 1) && (previous_role == 'owner')
        render(json: { data: 'You cannot have zero owner' }, status: :forbidden) && return

      else
        get_roles_list(previous_role).each do |role|
          @user.remove_role role, @obj
          @user.remove_edge(@obj, "is_#{role}_of")
        end
        get_roles_list(new_role).each do |role|
          @user.add_role role, @obj
          @user.add_edge(@obj, "is_#{role}_of")
        end
        # converting pending members to active members for private objects
        @obj.notif_pending_join_request_approved(@user) if (new_role == 'member') && @obj.try(:is_private?)
        render json: { data: 'New role set' }, status: :ok
      end
    else
      render json: { data: 'Malformed request' }, status: :unprocessable_entity
    end
  end

  def get_current_role(user, obj)
    if user.has_role? :owner, obj
      'owner'
    elsif user.has_role? :admin, obj
      'admin'
    elsif user.has_role? :member, obj
      'member'
    elsif user.has_role? :pending, obj
      'pending'
    end
  end

  # Returns all available roles for a given role
  def get_roles_list(role)
    if role == 'owner'
      %w[owner admin member]
    elsif role == 'admin'
      %w[admin member]
    elsif role == 'member'
      ['member']
    elsif role == 'pending'
      ['pending']
    else
      []
    end
  end

  def members_list
    members = if params[:status] == 'pending'
                @obj.pending_members
              else
                @obj.all_owners_admins_members
              end
    @pagy, @members = pagy(members.includes(%i[ressources interests skills sash]).all)
    render json: @members, each_serializer: Api::MembersSerializer, parent: @obj, root: 'members', adapter: :json
  end

  # We verify if the object is not archived if not we add the user/object relation
  # for Project Community and Space objects we send a confirmation email
  def join
    return archived_object if @obj.status == 'archived'
    return already_joined if current_user.roles.where(resource: @obj).any?
    return join_space if @obj.class.name == 'Space'
    return join_project_or_community if @obj.class.name == 'Project' || @obj.class.name == 'Community'
    join_object
  end

  # for a given object the user relation is deleted
  def leave
    if current_user.roles.where(resource: @obj).any?
      @obj.applied_roles.pluck(:name).each do |role_name|
        current_user.remove_role(role_name, @obj)
        current_user.remove_edge(@obj, "is_#{role_name}_of")
      end

      render json: { data: 'User has left the object' }, status: :ok
    else
      render json: { data: 'User is not a member of the object' }, status: :forbidden
    end
  end

  # Depending on a given param the user is either looked in the database or an email
  # is sent to the email given in the param
  # if the user is found in the database his role is changed and another type of email is sent
  def invite
    if params[:stranger_emails].present?
      params[:stranger_emails].each do |stranger_email|
        InviteStrangerEmailWorker.perform_async(current_user.id, @obj.class.name, @obj.id, stranger_email)
      end
      render json: { data: 'Stranger(s) invited' }, status: :ok
    elsif params[:user_ids].present?
      User.where(id: params[:user_ids]).each do |invitee|
        next if invitee.roles.where(name: :member, resource: @obj).any?

        invitee.add_role(:member, @obj)
        InviteUserEmailWorker.perform_async(current_user.id, @obj.class.name, @obj.id, invitee.id)
        invitee.add_edge(@obj, 'is_member_of')
      end
      render json: { data: 'User(s) invited' }, status: :ok
    end
  end

  private

  def member_authenticate_user!
    authenticate_user!
  end

  def can_remove?
    render(json: { data: 'Only an owner or admin can do this!' }, status: :forbidden) && return unless current_user.has_role?(:owner, @obj) || current_user.has_role?(:admin, @obj)
  end

  #Refacto Join method
  def archived_object
    render(json: { data: 'The object is archived and cannot be joined' }, status: :forbidden)
  end

  def already_joined
    render json: { data: 'User has already joined the object' }, status: :forbidden
  end

  def join_space
    if @obj.is_private
      @obj.notif_pending_member(current_user)
      @obj.notif_pending_join_request(current_user)
      current_user.add_role(:pending, @obj)
      current_user.add_edge(@obj, 'is_pending_of')
      render json: { data: "Successfully asked to join space. As it's a private space, please wait for the leaders to accept or reject your request" }, status: :ok
    else
      # add check to remove pending role if it exists
      @obj.notif_new_member(current_user)
      current_user.add_role(:member, @obj)
      current_user.add_edge(@obj, 'is_member_of')
      render json: { data: 'Successfully joined!' }, status: :ok
      UserJoinedEmailWorker.perform_async(@obj.class.name, @obj.id, current_user.id)
    end    
    # hook_new_member(@obj.externalhooks, current_user, @obj) if @obj.class.name == 'Project'
  end

  def join_project_or_community
    if @obj.is_private
      @obj.notif_pending_member(current_user)
      @obj.notif_pending_join_request(current_user)
      current_user.add_role(:pending, @obj)
      current_user.add_edge(@obj, 'is_pending_of')
      render json: { data: "Successfully asked to join project. As it's a private project, please wait for the leaders to accept or reject your request" }, status: :ok
    else
      # add check to remove pending role if it exists
      @obj.notif_new_member(current_user)
      current_user.add_role(:member, @obj)
      current_user.add_edge(@obj, 'is_member_of')
      render json: { data: 'Successfully joined!' }, status: :ok
      UserJoinedEmailWorker.perform_async(@obj.class.name, @obj.id, current_user.id)
    end
    hook_new_member(@obj.externalhooks, current_user, @obj) if @obj.class.name == 'Project'
  end

  def join_object
    @obj.notif_new_member(current_user)
    current_user.add_role(:member, @obj)
    current_user.add_edge(@obj, 'is_member_of')
    render json: { data: 'Successfully joined!' }, status: :ok
    hook_new_member(@obj.externalhooks, current_user, @obj) if @obj.class.name == 'Challenge'
  end

end

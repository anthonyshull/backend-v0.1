# frozen_string_literal: true

class Api::SpacesController < ApplicationController
  # Before action validation are applied on different methods in order to
  # prevent events that are not destined to be executed, this also prevents
  # repeating the code on each method
  before_action :authenticate_user!, only: %i[create update destroy my_spaces can_create]
  before_action :find_space, except: %i[index create my_spaces get_id_from_short_title short_title_exist recommended can_create]
  before_action :find_program, only: %i[attach remove]
  before_action :set_obj, except: %i[index create destroy short_title_exist get_id_from_short_title recommended can_create]
  before_action :is_admin, only: %i[update invite upload_banner remove_banner
                                    update_member remove_member]

  include Api::Follow
  include Api::Members
  include Api::Upload
  include Api::Relations
  include Api::Utils
  include Api::Recommendations
  include Api::Faq
  include Api::ExternalLinks
  include Api::Resources

  def index
    @pagy, @spaces = pagy(
      Space
        .where
        .not(status: 'draft')
        .includes(%i[
                    banner_attachment
                    avatar_attachment
                    feed
                    documents_attachments
                    programs
                  ]).all
    )
    render json: @spaces
  end

  # We only need to display Projects or Challenges info for now
  # TODO display both Projects and Challenges info
  # Commented part of the method is for a part of the TODO but not fully working. Still need to do some refacto.
  def affiliates
    space = Space.find(params[:id])
    response = {}
    if params[:affiliate_type].present?
      affiliate_type_sym = params[:affiliate_type].downcase.pluralize.to_sym
      response[affiliate_type_sym] = space.affiliations.includes([:affiliate]).where(affiliate_type: params[:affiliate_type]).order(created_at: :asc).map(&:affiliate)
    # else
    #   affiliate_types = space.affiliations.pluck(:affiliate_type).uniq
    #   affiliate_types.each do |affiliate_type|
    #     response[affiliate_type.downcase.pluralize.to_sym] = space.affiliations.includes([:affiliate]).where(affiliate_type: affiliate_type).order(created_at: :asc).map(&:affiliate)
    #   end
    end
    if affiliate_type_sym == :projects
        render json: Panko::Response.new(
          projects: Panko::ArraySerializer.new(
            response[affiliate_type_sym],
            each_serializer: Api::SharedProjectSerializer,
            context: { current_user: current_user, space_id: @space.id }
          )
        )
      elsif affiliate_type_sym == :challenges
        render json: Panko::Response.new(
          challenges: Panko::ArraySerializer.new(
            response[affiliate_type_sym],
            each_serializer: Api::SharedChallengeSerializer,
            context: { current_user: current_user, space_id: @space.id }
          )
        )
      # else
      #   render json: Panko::Response.new(
      #     projects: Panko::ArraySerializer.new(
      #       response[affiliate_types],
      #       each_serializer: Api::SharedProjectSerializer,
      #       context: { current_user: current_user, space_id: @space.id }
      #     ),
      #     challenges: Panko::ArraySerializer.new(
      #       response[affiliate_types],
      #       each_serializer: Api::SharedChallengeSerializer,
      #       context: { current_user: current_user, space_id: @space.id }
      #     )
      #   )
      end
  end

  def my_spaces
    @spaces = current_user.spaces
    @pagy, @spaces = pagy_array(@spaces.uniq)
    render json: @spaces
  end

  def can_create
    if current_user.has_role? :space_creator
      render json: { data: 'Authorized' }, status: :ok
    else
      render json: { data: 'Forbidden' }, status: :forbidden
    end
  end

  def create
    render(json: { data: 'Forbidden' }, status: :forbidden) && return unless current_user.has_role? :space_creator

    @space = Space.new(space_params)
    @space.status = 'draft'
    @space.creator_id = current_user.id
    @space.users << current_user

    if @space.save
      @space.create_faq
      current_user.add_role :owner, @space
      current_user.add_role :admin, @space
      current_user.add_role :member, @space
      render json: { id: @space.id, short_title: @space.short_title, data: 'Success' }, status: :created
    else
      render json: { data: 'Something went wrong' }, status: :unprocessable_entity
    end
  end

  def show
    render json: @space, show_objects: true
  end

  def getid
    @space = Space.where(short_title: params[:nickname]).first
    render json: { id: @space.id, data: 'Success' }, status: :ok
  end

  def update
    if @space.update(space_params)
      render json: @space, status: :ok
    else
      render json: { data: 'Something went wrong :(' }, status: :unprocessable_entity
    end
  end

  def destroy
    # NOTE: a space with 1 user is considered a test space, and can be destroyed
    if @space.users.count == 1 && current_user.has_role?(:owner, @space)
      @space.destroy
      render json: { data: "Space id:#{@space.id} destroyed" }, status: :ok
    else
      @space.archived!
      render json: { data: "Space id:#{@space.id} archived" }, status: :ok
    end
  end

  # This method adds programs to a space and changes its status
  def attach
    if @space.programs.include?(@program)
      render json: { data: "Program id:#{@program.id} is already attached" }, status: :ok
    else
      @space.programs << @program
      render json: { data: "Program id:#{@program.id} attached" }, status: :ok
    end
  end

  # The object relation is removed with no status change
  def remove
    if @space.programs.include?(@program)
      @space.programs.delete(@program)
      render json: { data: "Program id:#{@program.id} removed" }, status: :ok
    else
      render json: { data: "Program id:#{@program.id} is not attached" }, status: :not_found
    end
  end

  def index_programs
    programs = @space.programs.order(id: :desc)

    @pagy, records = pagy(programs)
    render json: records, each_serializer: Api::ProgramSerializer, root: 'program', adapter: :json
  end

  def index_challenges
    challenges = Challenge
                 .joins(:program)
                 .where("programs.space_id = #{@space.id}")
                 .order(id: :desc)

    @pagy, records = pagy(challenges)
    render json: records, each_serializer: Api::ChallengeSerializer, root: 'challenges', adapter: :json
  end

  def index_projects
    # Don't take archived projects into account
    projects = Project
               .joins(challenges: :program)
               .distinct
               .where("programs.space_id = #{@space.id}")
               .where.not(status: :archived)
               .order(id: :desc) # can't use created_at since some records don't have it

    @pagy, records = pagy(projects)
    render json: Panko::Response.new(
      projects: Panko::ArraySerializer.new(
        records,
        each_serializer: Api::SharedProjectSerializer,
        context: { current_user: current_user, program_id: @space.id }
      )
    )
  end

  def index_needs
    # Only take accepted projects into account
    needs = Need
            .joins(project: [{ challenges_projects: [{ challenge: [{ program: :space }] }] }])
            .distinct
            .where("programs.space_id = #{@space.id}")
            .where(challenges_projects: { project_status: ChallengesProject.project_statuses[:accepted] })
            .order(id: :desc) # can't use created_at since some records don't have it

    @pagy, records = pagy(needs)
    render json: Panko::Response.new(
      needs: Panko::ArraySerializer.new(
        records,
        each_serializer: Api::SharedNeedSerializer,
        context: { current_user: current_user }
      )
    )
  end

  private

  def find_space
    @space = Space.find_by(id: params[:id])
    render json: { data: "Space id:#{params[:id]} not found" }, status: :not_found if @space.nil?
  end

  def find_program
    @program = Program.find_by(id: params[:program_id])
    render json: { data: "Program id:#{params[:program_id]} was not found" }, status: :not_found if @program.nil?
  end

  def set_obj
    @obj = @space
  end

  def space_params
    params
      .require(:space)
      .permit(
        :code_of_conduct,
        :contact_email,
        :custom_challenge_name,
        :description_fr,
        :description,
        :enablers_fr,
        :enablers,
        :end_date,
        :home_header,
        :home_info,
        :launch_date,
        :meeting_information,
        :onboarding_steps,
        :ressources,
        :short_description_fr,
        :short_description,
        :short_title_fr,
        :short_title,
        :show_featured_challenges,
        :show_featured_needs,
        :show_featured_programs,
        :show_featured_projects,
        :space_type,
        :status,
        :title,
        :title_fr,
        available_tabs: {},
        selected_tabs: {},
        show_featured: {}
      )
  end
end

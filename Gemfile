# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.2'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.1', '>= 6.1.3.1'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma'
# Use Redis adapter to run Action Cable in production
gem 'redis'
# Use ActiveModel has_secure_password
gem 'bcrypt'

# gem 'devise_token_auth', github: 'lynndylanhurley/devise_token_auth', ref: 'c92258038c05fcc8f6a0374ccce2e63b9f8d5312'
gem 'devise_token_auth'

gem 'devise', '~> 4.7'

gem 'devise-async'

gem 'rolify', github: 'RolifyCommunity/rolify', ref: '03dcfd251a37149723a712f6760df053219525d8'

gem 'multipart-post'

gem 'dotenv-rails', groups: %i[development test]

gem 'bullet'

# Transactional email
gem 'postmark-rails'

# Required for the API to serve assets such as images
gem 'sass-rails'
gem 'uglifier'

gem 'active_model_serializers'
gem 'panko_serializer'
# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Reduces boot times through caching; required in config/boot.rb
# gem 'bootsnap', '>= 1.1.0', require: false

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors', require: 'rack/cors'

# Slack
gem 'slack-notifier'

# MJML Template Mailer
gem 'mjml-rails', '~> 4.4', '>= 4.4.1'

group :production do
  # Rails metrics by Skylight.io
  gem 'skylight'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Algolia search engine
# TODO: upgrade this when new 2.0-compatible version is released
gem 'algoliasearch-rails'

gem 'seedbank'

gem 'sidekiq', require: ['sidekiq', 'sidekiq/web']
# allow repeating task scheduling with cron
# and show the web interface.
gem 'sidekiq-cron'

gem 'mini_magick'

gem 'aws-sdk-s3', require: false

# wrapper for MailChimp
gem 'gibbon'

gem 'et-orbi'

gem 'pagy'

gem 'merit'

gem 'paper_trail'

gem 'factory_bot_rails'

gem 'notification-pusher-actionmailer'
gem 'notifications-rails', '3.0.2'

gem 'mini_racer'

group :development do
  gem 'spring-commands-rspec'
end

# Adding Swagger and rspec
group :development, :test do
  gem 'pry'

  gem 'database_cleaner'
  gem 'ffaker', require: true
  gem 'rails-controller-testing'
  gem 'rspec-rails', '~> 4.0'
  gem 'shoulda'
  gem 'simplecov'
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
end

group :test do
  gem 'rspec-sidekiq'
  gem 'timecop'
end

# Adding GeoCoding capacity
gem 'geocoder', '>= 1.6.3'

# Adding ELK logstash system
gem 'logstasher'
gem 'logstash-logger'

# Graph QL capacity
gem 'graphql'

# Adding HTTP request handler
gem 'platform-api'

# sentry.io application error monitoring
# Make working with jsonb quite pleasant
gem 'attr_json'

# Generating image variants
gem 'image_processing', '~> 1.2'

gem 'sentry-rails'
gem 'sentry-ruby'
gem 'sentry-sidekiq'

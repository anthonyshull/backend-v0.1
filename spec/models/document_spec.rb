# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Document, type: :model do
  describe 'validation' do
    it 'should have valid factory' do
      expect(build(:document)).to be_valid
    end
  end
end

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe NeedsProject, type: :model do
  describe 'associations' do
    it { should belong_to(:need) }
    it { should belong_to(:project) }
  end
end

# frozen_string_literal: true

require 'rails_helper'
require_relative 'shared_examples/feedable'

RSpec.describe Challenge, type: :model do
  it_behaves_like 'a model that can have a feed', :challenge

  describe 'associations' do
    it { should have_and_belong_to_many(:skills) }
    it { should have_and_belong_to_many(:interests) }
    it { should have_one(:feed) }
    it { should have_one(:faq) }

    it 'creates an associated faq after creation' do
      challenge = Challenge.create

      expect(challenge.faq).to be_present
    end
  end

  describe 'needs_count' do
    it 'counts needs from accepted projects only' do
      challenge = create(:challenge)
      project_one = create(:project)
      challenge.projects << project_one
      project_one.needs << create(:need)
      project_two = create(:project)
      challenge.projects << project_two
      project_two.needs << create(:need)
      challenge.challenges_projects.update_all(project_status: :accepted)
      project_three = create(:project)
      project_three.needs << create(:need)
      challenge.projects << project_three
      expect(challenge.needs_count).to eq(2)
    end
  end

  # describe 'notifications' do
  #   it "sends a notification when status changes from 'draft' to 'soon'" do
  #     challenge = create(:challenge, status: :draft)
  #     expect(challenge).to receive(:notif_soon_challenge)
  #     challenge.soon!
  #   end

  #   it "does not send a notification if status is 'soon' and another attribute was updated" do
  #     challenge = create(:challenge, status: :soon)
  #     expect(challenge).to_not receive(:notif_soon_challenge)
  #     challenge.update(title: 'bananas')
  #   end

  #   it "sends a notification when status changes from 'soon' to 'active'" do
  #     challenge = create(:challenge, status: :soon)
  #     expect(challenge).to receive(:notif_start_challenge)
  #     challenge.active!
  #   end

  #   it "does not send a notification if status is 'active' and another attribute was updated" do
  #     challenge = create(:challenge, status: :active)
  #     expect(challenge).to_not receive(:notif_start_challenge)
  #     challenge.update(title: 'bananas')
  #   end

  #   it "sends a notification when status changes from 'active' to 'completed'" do
  #     challenge = create(:challenge, status: :active)
  #     expect(challenge).to receive(:notif_end_challenge)
  #     challenge.completed!
  #   end

  #   it "does not send a notification if status is 'completed' and another attribute was updated" do
  #     challenge = create(:challenge, status: :completed)
  #     expect(challenge).to_not receive(:notif_end_challenge)
  #     challenge.update(title: 'bananas')
  #   end
  # end

  it 'responds to banner_url_sm' do
    expect(described_class.new).to respond_to(:banner_url_sm)
  end

  describe '#add_users_from_projects' do
    it "adds project users to the challenge's users" do
      challenge = create(:challenge)
      confirmed_project_users = []
      create_list(:project, 2).each do |project|
        user = create(:user)
        confirmed_project_users << user
        user.add_role(:member, project)
        challenge.projects << project
        challenge.challenges_projects.update(project_status: 'accepted')
      end

      unconfirmed_project = create(:project)
      unconfirmed_project_user = create(:user)
      unconfirmed_project_user.add_role(:member, unconfirmed_project)
      challenge.projects << unconfirmed_project

      challenge.add_users_from_projects

      # includes
      # 1 factory-created challenge creator (1x),
      # 1 factory-created creator per project (2x),
      # and 1 test-created user for each `accepted` challenge_project (2x)
      expect(challenge.users).to include(*confirmed_project_users)
      expect(challenge.users).to_not include(unconfirmed_project_user)
    end
  end

  describe '#projects_count' do
    it 'is the number of accepted projects' do
      challenge = create(:challenge)
      expect(challenge.projects_count).to eq(0)
      project = create(:project)
      challenge.projects << project
      challenge.challenges_projects.first.update(project_status: 'accepted')
      expect(challenge.projects_count).to eq(1)
    end
  end

  describe 'logo_url_sm' do
    before do
      @challenge = build(:challenge)
    end

    it 'picks a default avatar image' do
      expect(@challenge.logo_url_sm).to match(%r{http://localhost:3001/assets/default-avatar-\w*.png})
    end

    it 'calls default_logo_url if avatar.attachment is not available' do
      expect(@challenge).to receive(:default_logo_url).with(no_args).at_least(:once)
      @challenge.logo_url_sm
      @challenge.logo_url
    end

    it "uses the avatar image if it's present" do
      @challenge.avatar.attach(
        io: File.open(Rails.root.join('spec', 'test-image.png')),
        filename: 'test-image'
      )

      expect(@challenge).to receive(:avatar_variant_url).with(resize: '40x40^')
      @challenge.logo_url_sm
    end

    it "uses the avatar image if it's present" do
      @challenge.avatar.attach(
        io: File.open(Rails.root.join('spec', 'test-image.png')),
        filename: 'test-image'
      )

      expect(@challenge).to receive(:avatar_variant_url).with(resize: '200x200^')
      @challenge.logo_url
    end

    it "uses the avatar image if it's present" do
      @challenge.avatar.attach(
        io: File.open(Rails.root.join('spec', 'test-image.svg')),
        filename: 'test-image'
      )

      expect(@challenge).to receive(:avatar_blob_url).at_least(:once)
      @challenge.logo_url_sm
    end
  end

  describe 'banner_url_sm' do
    before do
      @challenge = build(:challenge)
    end

    it 'picks a default banner image' do
      expect(@challenge.banner_url_sm).to match(%r{http://localhost:3001/assets/default-challenge-\w*.jpg})
    end

    it 'calls default_banner_url if banner.attachment is not available' do
      expect(@challenge).to receive(:default_banner_url).with(no_args)
      @challenge.banner_url
    end

    it "uses the banner image if it's present" do
      @challenge.banner.attach(
        io: File.open(Rails.root.join('spec', 'test-image.png')),
        filename: 'test-image'
      )

      expect(@challenge).to receive(:banner_variant_url).with(resize: '100x100^')
      @challenge.banner_url_sm
    end

    it "uses the banner image if it's present" do
      @challenge.banner.attach(
        io: File.open(Rails.root.join('spec', 'test-image.png')),
        filename: 'test-image'
      )

      expect(@challenge).to receive(:banner_variant_url).with(resize: '400x400^')
      @challenge.banner_url
    end

    it "uses the banner image if it's present" do
      @challenge.banner.attach(
        io: File.open(Rails.root.join('spec', 'test-image.svg')),
        filename: 'test-image'
      )

      expect(@challenge).to receive(:banner_blob_url)
      @challenge.banner_url
    end
  end
end

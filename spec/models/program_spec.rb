# frozen_string_literal: true

require 'rails_helper'
require_relative 'shared_examples/feedable'

RSpec.describe Program, type: :model do
  it_behaves_like 'a model that can have a feed', :program

  describe 'associations' do
    it { should have_many(:challenges) }
    it { should have_one(:feed) }
  end

  describe 'validation' do
    let!(:program) { create(:program) }
    it 'should have valid factory' do
      expect(build(:program)).to be_valid
    end

    # it "should have unique short title" do
    #   program2 = build(:program, :short_title => program.short_title)
    #   expect(build(:program)).to be_valid
    #   expect(program2.errors[:short_title]).to include("has already been taken")
    # end
  end

  # describe 'notifications' do
  #   it "sends a notification when status changes from 'draft' to 'soon'" do
  #     program = create(:program, status: :draft)
  #     expect(program).to receive(:notif_soon_program)
  #     program.soon!
  #   end

  #   it "sends a notification when status changes from 'soon' to 'active'" do
  #     program = create(:program, status: :soon)
  #     expect(program).to receive(:notif_start_program)
  #     program.active!
  #   end

  #   it "sends a notification when status changes from 'active' to 'completed'" do
  #     program = create(:program, status: :active)
  #     expect(program).to receive(:notif_end_program)
  #     program.completed!
  #   end
  # end

  describe 'logo_url_sm' do
    before do
      @program = build(:program)
    end

    it 'picks a default avatar image' do
      expect(@program.logo_url_sm).to match(%r{http://localhost:3001/assets/default-avatar-\w*.png})
    end

    it 'calls default_logo_url if avatar.attachment is not available' do
      expect(@program).to receive(:default_logo_url).with(no_args).at_least(:once)
      @program.logo_url_sm
      @program.logo_url
    end

    it "uses the avatar image if it's present" do
      @program.avatar.attach(
        io: File.open(Rails.root.join('spec', 'test-image.png')),
        filename: 'test-image'
      )

      expect(@program).to receive(:avatar_variant_url).with(resize: '40x40^')
      @program.logo_url_sm
    end

    it "uses the avatar image if it's present" do
      @program.avatar.attach(
        io: File.open(Rails.root.join('spec', 'test-image.png')),
        filename: 'test-image'
      )

      expect(@program).to receive(:avatar_variant_url).with(resize: '200x200^')
      @program.logo_url
    end

    it "uses the avatar image if it's present" do
      @program.avatar.attach(
        io: File.open(Rails.root.join('spec', 'test-image.svg')),
        filename: 'test-image'
      )

      expect(@program).to receive(:avatar_blob_url).at_least(:once)
      @program.logo_url_sm
    end
  end

  describe 'banner_url_sm' do
    before do
      @program = build(:program)
    end

    it 'picks a default banner image' do
      expect(@program.banner_url_sm).to match(%r{http://localhost:3001/assets/default-program-\w*.jpg})
    end

    it 'calls default_banner_url if banner.attachment is not available' do
      expect(@program).to receive(:default_banner_url).with(no_args)
      @program.banner_url
    end

    it "uses the banner image if it's present" do
      @program.banner.attach(
        io: File.open(Rails.root.join('spec', 'test-image.png')),
        filename: 'test-image'
      )

      expect(@program).to receive(:banner_variant_url).with(resize: '100x100^')
      @program.banner_url_sm
    end

    it "uses the banner image if it's present" do
      @program.banner.attach(
        io: File.open(Rails.root.join('spec', 'test-image.png')),
        filename: 'test-image'
      )

      expect(@program).to receive(:banner_variant_url).with(resize: '400x400^')
      @program.banner_url
    end

    it "uses the banner image if it's present" do
      @program.banner.attach(
        io: File.open(Rails.root.join('spec', 'test-image.svg')),
        filename: 'test-image'
      )

      expect(@program).to receive(:banner_blob_url)
      @program.banner_url
    end
  end

  describe 'Challenge mechanics' do
    before do
      @program = build(:program)
      @project1 = create(:project)
      @need1 = create(:need, project: @project1)
      @project1.needs << @need1
      @project2 = create(:project)
      @need2 = create(:need, project: @project2)
      @project2.needs << @need2
      @project3 = create(:project)
      @need3 = create(:need, project: @project3)
      @project3.needs << @need3
      @challenge = create(:challenge, program: @program)
      @challenge.projects << @project1 << @project2 << @project3
      @challenge.accept_project(@project1)
      @challenge.accept_project(@project2)
    end

    it 'has the correct project count' do
      expect(@program.projects_count).to eq 2
    end

    it 'has the correct needs_count' do
      expect(@program.needs_count).to eq 2
    end
  end
end

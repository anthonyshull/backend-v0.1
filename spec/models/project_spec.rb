# frozen_string_literal: true

require 'rails_helper'
require_relative 'shared_examples/feedable'

RSpec.describe Project, type: :model do
  it_behaves_like 'a model that can have a feed', :project

  describe 'associations' do
    it { should have_many(:challenges) }
    it { should have_one(:feed) }
    it { should have_and_belong_to_many(:skills) }
    it { should have_and_belong_to_many(:interests) }
  end

  describe 'validation' do
    let!(:project) { create(:project, creator_id: create(:user).id) }
    it 'should have valid factory' do
      expect(project).to be_valid
    end
  end

  describe 'members' do
    it 'does something' do
      project = create(:project)
      expect(project.members).to include(project.creator)
      expect(project.members.size).to eq(1)
    end
  end
end

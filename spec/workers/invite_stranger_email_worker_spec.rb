# frozen_string_literal: true

require 'rails_helper'
# require 'sidekiq/testing'

RSpec.describe InviteStrangerEmailWorker, type: :worker do
  describe 'testing worker' do
    it 'should sends to right email' do
      owner = build(:user)
      object = build(:project, creator_id: owner.id)
      stranger = 'some@some.com'
      mail = InviteStrangerMailer.invite_stranger(owner, object, stranger)
      expect(mail.to).to eq [stranger]
    end

    it 'should renders the subject' do
      owner = build(:user)
      object = build(:project, creator_id: owner.id)
      stranger = build(:user)
      mail = InviteStrangerMailer.invite_stranger(owner, object, stranger)
      expect(mail.subject).to eq 'You have been invited to join a project'
    end
  end
end

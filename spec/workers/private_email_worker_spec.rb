# frozen_string_literal: true

require 'rails_helper'
# require 'sidekiq/testing'

RSpec.describe PrivateEmailWorker, type: :worker do
  describe 'testing worker' do
    # it "should respond to #perform" do
    #   expect(PrivateMailer.send_private_email(from, to, object, content)).to respond_to(:perform)
    # end

    # describe "perform" do
    #   before do
    #     Sidekiq::Worker.clear_all
    #   end
    # end

    it 'should sends from right email' do
      ENV['JOGL_NOTIFICATIONS_EMAIL'] = 'test@jogl.io'
      from_user = create(:user)
      to_user = create(:user)
      object = create(:project, creator_id: from_user.id)
      content = 'skjhgsalkjf'
      mail = PrivateMailer.send_private_email(from_user, to_user, object, content)
      expect(mail.to).to eq [to_user.email]
      expect(mail.from[0]).to match(/@jogl\.io/)
    end

    it 'should renders the subject' do
      from_user = create(:user)
      to_user = create(:user)
      object = create(:project, creator_id: from_user.id)
      content = 'skjhgsalkjf'
      mail = PrivateMailer.send_private_email(from_user, to_user, object, content)
      expect(mail.subject).to eq "#{from_user.first_name} sent you a message"
    end
  end
end

# frozen_string_literal: true

FactoryBot.define do
  factory :dataset do
    title { 'MyString' }
    description { 'MyString' }
    type { 'Datasets::CustomDataset' }
    url { 'https://some.url.com' }
    author
  end
end

# frozen_string_literal: true

FactoryBot.define do
  factory :ressource do
    ressource_name { FFaker::Name.name }
  end
end

# frozen_string_literal: true

require 'rails_helper'
require_relative 'shared_examples/clappable'
require_relative 'shared_examples/followable'
require_relative 'shared_examples/saveable'
require_relative 'shared_examples/memberable'
require_relative 'shared_examples/recommendable'
require_relative 'shared_examples/linkable'
require_relative 'shared_examples/private_object_membership'

RSpec.describe Api::CommunitiesController, type: :controller do
  context 'Examples' do
    describe 'community relations' do
      it_behaves_like 'an object with clappable', :community
      it_behaves_like 'an object with followable', :community
      it_behaves_like 'an object with saveable', :community
    end

    describe 'Community' do
      it_behaves_like 'an object with members', :community
      it_behaves_like 'a private object with members', :community
      it_behaves_like 'an object with recommendations', :community
      it_behaves_like 'an object with links', :community
    end
  end

  context 'Communities specs' do
    before do
      @user = create(:confirmed_user)
      sign_in @user
      @community = create(:community)
      # Community.create(
      #   title: FFaker::Movie.title,
      #   short_title: FFaker::Internet.user_name,
      #   logo_url: FFaker::Avatar.image,
      #   description: FFaker::DizzleIpsum.paragraphs,
      #   short_description: FFaker::DizzleIpsum.paragraph,
      #   latitude: rand(-90.000000000...90.000000000),
      #   longitude: rand(-180.000000000...180.000000000),
      #   status: 0,
      #   is_private: true,
      #   creator_id: @user.id
      # )
    end

    describe '#index' do
      it 'should return list of community' do
        get :index
        expect(response).to have_http_status :ok
      end
    end

    describe '#my communities' do
      context 'Fetching My communites with logged in user' do
        it 'should return list of Users community' do
          get :my_communities
          expect(response).to have_http_status :ok
        end
      end

      context 'Fetching My communites without logged in user' do
        it 'should return list of Users community' do
          sign_out @user
          get :my_communities
          expect(response).to have_http_status :unauthorized
        end
      end
    end

    describe '#create' do
      context 'duplicate short title error' do
        it 'creates a community' do
          post :create, params: { community: @community.attributes }
          expect(response.status).to eq 422
        end
      end

      context 'creating community with logged in user' do
        it 'creates a community' do
          @community.short_title = FFaker::Name.first_name
          post :create, params: { community: @community.attributes }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 201
          expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: 'Community', targetable_node_id: json_response['id'], relation_type: 'is_author_of')).to exist
          expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: 'Community', targetable_node_id: json_response['id'], relation_type: 'is_member_of')).to exist
          expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: 'Community', targetable_node_id: json_response['id'], relation_type: 'is_admin_of')).to exist
          expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: 'Community', targetable_node_id: json_response['id'], relation_type: 'is_owner_of')).to exist
        end
      end

      context 'creating community without logged in user' do
        it 'return unauthorized error' do
          @community.short_title = FFaker::Name.first_name
          sign_out @user
          post :create, params: { community: @community.attributes }
          expect(response.status).to eq 401
        end
      end
    end

    describe '#show' do
      it 'should return community' do
        get :show, params: { id: @community.id }
        expect(response.status).to eq 200
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: @community.class.name, targetable_node_id: @community.id, relation_type: 'has_visited')).to exist
      end
    end

    describe '#update' do
      context 'Updating community with logged in user and with admin role' do
        it 'Updates a community' do
          @user.add_role :admin, @community
          put :update, params: { community: { description: 'New Description' }, id: @community.id }
          expect(response.status).to eq 200
        end
      end

      context 'User cant update because user is not an admin' do
        it 'Should throw Forbidden error' do
          put :update, params: { community: { description: 'New Description' }, id: @community.id }
          expect(response.status).to eq 403
        end
      end

      context 'without logged in user' do
        it 'Should throw unauthorized error' do
          sign_out @user
          put :update, params: { community: { description: 'New Description' }, id: @community.id }
          expect(response.status).to eq 401
        end
      end
    end

    describe '#destroy' do
      context 'with logged in user' do
        it 'Should Delete the community' do
          delete :destroy, params: { id: @community.id }
          expect(response.status).to eq 200
        end
      end

      context 'without logged in user' do
        it 'Should not delete the community' do
          sign_out @user
          delete :destroy, params: { id: @community.id }
          expect(response.status).to eq 401
        end
      end
    end
  end
end

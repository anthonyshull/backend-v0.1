# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'an object with members' do |factory|
  before do
    @object = create(factory)
    if @object.has_privacy?
      @object.is_private = false
      @object.save
    end
  end

  context 'Signed in' do
    before(:each) do
      @user = create(:confirmed_user, first_name: 'Kaya', last_name: 'Yaya')
      sign_in @user
    end

    describe '#join' do
      it "should join the #{described_class}" do
        put :join, params: { id: @object.id }
        expect(response).to have_http_status :ok
        expect(@object.users.count).to eq 2
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'is_member_of')).to exist
      end

      it 'should trigger a notification' do
        expect do
          put :join, params: { id: @object.id }
        end.to change(Notification, :count).by(1)
        notif = Notification.where(object_type: @object.class.name, object_id: @object.id, type: 'new_member').first
        expect(notif).to be_truthy
        expect(notif.category).to match('membership')
        expect(notif.target).to be == @object.users.first
        expect(notif.metadata[:author_id]).to be(@user.id)
        expect(notif.subject_line).to match(/Kaya Yaya joined your .+: #{@object.title}/)
      end
    end

    describe '#leave' do
      before do
        @user.add_role(:member, @object)
      end

      it "should leave the #{described_class}" do
        expect(@object.users.count).to eq 2
        put :leave, params: { id: @object.id }
        expect(response).to have_http_status :ok
        expect(@object.users.count).to eq 1
        expect(RecsysDatum.where(sourceable_node: @user, targetable_node: @object, relation_type: 'is_member_of')).not_to exist
      end
    end

    describe '#invite' do
      it "should invite existing users to join the #{described_class}" do
        @user.add_role(:admin, @object)
        @user2 = create(:confirmed_user)

        expect(InviteUserEmailWorker).to receive(:perform_async).once.with(@user.id, @object.class.name, @object.id, @user2.id)

        put :invite, params: { id: @object.id, user_ids: [@user2.id] }

        expect(response).to have_http_status :ok
      end

      it "should invite strangers to join the #{described_class}" do
        @user.add_role(:admin, @object)
        stranger_1 = build(:confirmed_user)
        stranger_2 = build(:confirmed_user)

        expect(InviteStrangerEmailWorker).to receive(:perform_async).once.with(@user.id, @object.class.name, @object.id, stranger_1.email)
        expect(InviteStrangerEmailWorker).to receive(:perform_async).once.with(@user.id, @object.class.name, @object.id, stranger_2.email)

        put :invite, params: { id: @object.id, stranger_emails: [ stranger_1.email, stranger_2.email ] }

        expect(response).to have_http_status :ok
      end
    end


    describe '#has_membership' do
      before do
        @user.add_role(:member, @object)
      end

      it 'should answer true' do
        get :has_membership, params: { id: @object.id, user_id: @user.id }
        expect(response).to have_http_status :found
      end

      it 'should answer false' do
        @user.remove_role :member, @object
        get :has_membership, params: { id: @object.id, user_id: @user.id }
        expect(response).to have_http_status :not_found
      end
    end

    # only works for models with resourcify
    describe '#members_count' do
      it 'returns a count of resource members' do
        challenge = create(:challenge)
        expect(challenge.members_count).to eq(1)
      end
    end

    describe '#members_list' do
      before do
        @user.add_role :admin, @object

        @user2 = create(:confirmed_user)
        @user2.add_role :member, @object
      end

      it "should get the members of the #{described_class}" do
        user_common = create(:confirmed_user)
        @user.owned_relations.create(resource: user_common, follows: true)
        @user2.owned_relations.create(resource: user_common, follows: true)
        get :members_list, params: { id: @object.id }
        expect(response).to have_http_status :ok
        json_response = JSON.parse(response.body)
        expect(json_response['members'].count).to eq 3
        expect(json_response['members'].find { |u| u['id'] == @user.id } ['stats']['mutual_count']).to eq(1)
        expect(json_response['members'].find { |u| u['id'] == @user2.id } ['stats']['mutual_count']).to eq(1)
      end

      it "should not return users not members of the #{described_class}" do
        get :members_list, params: { id: @object.id }
        expect(response).to have_http_status :ok
        json_response = JSON.parse(response.body)
        expect(json_response['members'].count).to eq 3
      end

      it "should paginate the members of the #{described_class}" do
        get :members_list, params: { id: @object.id, items: 1, page: 1 }
        expect(response).to have_http_status :ok
        json_response = JSON.parse(response.body)
        expect(json_response['members'].count).to eq 1
      end
    end

    describe '#update_member' do
      before do
        @user.add_role :member, @object
        @user.add_role :admin, @object
        @user.add_role :owner, @object

        @user2 = create(:confirmed_user)
        @user2.add_role :member, @object
      end

      it 'should not let you update to owner if you are not owner' do
        @user.remove_role :owner, @object
        put :update_member, params: { id: @object.id, user_id: @user2.id, previous_role: 'member', new_role: 'owner' }
        expect(response).to have_http_status :forbidden
        expect(@user2.has_role?(:admin, @object)).to be_falsey
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user2.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'is_admin_of')).not_to exist
      end

      it 'should let you update to admin if you are admin' do
        @user.remove_role :owner, @object
        expect(@user.has_role?(:admin, @object)).to be_truthy
        expect(@user.has_role?(:owner, @object)).to be_falsey
        put :update_member, params: { id: @object.id, user_id: @user2.id, previous_role: 'member', new_role: 'admin' }
        expect(response).to have_http_status :ok
        expect(@user2.has_role?(:admin, @object)).to be_truthy
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user2.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'is_admin_of')).to exist
      end

      it 'should not let you update to admin if you are member' do
        @user.remove_role :admin, @object
        @user.remove_role :owner, @object
        put :update_member, params: { id: @object.id, user_id: @user2.id, previous_role: 'member', new_role: 'admin' }
        expect(response).to have_http_status :forbidden
        expect(@user2.has_role?(:admin, @object)).to be_falsey
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user2.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'is_admin_of')).not_to exist
      end

      it 'should not let you update to admin if you are member' do
        @user.remove_role :admin, @object
        @user.remove_role :owner, @object
        put :update_member, params: { id: @object.id, user_id: @user2.id, previous_role: 'member', new_role: 'owner' }
        expect(response).to have_http_status :forbidden
        expect(@user2.has_role?(:admin, @object)).to be_falsey
        expect(@user2.has_role?(:owner, @object)).to be_falsey
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user2.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'is_admin_of')).not_to exist
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user2.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'is_owner_of')).not_to exist
      end

      it "should update from member to admin of the #{described_class}" do
        put :update_member, params: { id: @object.id, user_id: @user2.id, previous_role: 'member', new_role: 'admin' }
        expect(response).to have_http_status :ok
        expect(@user2.has_role?(:admin, @object)).to be_truthy
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user2.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'is_admin_of')).to exist
      end

      it "should update from admin to member of the #{described_class}" do
        @user2.add_role :admin, @object
        put :update_member, params: { id: @object.id, user_id: @user2.id, previous_role: 'admin', new_role: 'member' }
        expect(response).to have_http_status :ok
        expect(@user2.has_role?(:admin, @object)).to be_falsey
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user2.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'is_member_of')).to exist
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user2.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'is_admin_of')).not_to exist
      end

      it "should update from owner to member of the #{described_class}" do
        @user2.add_role :admin, @object
        @user2.add_role :owner, @object
        put :update_member, params: { id: @object.id, user_id: @user2.id, previous_role: 'owner', new_role: 'member' }
        expect(response).to have_http_status :ok
        expect(@user2.has_role?(:admin, @object)).to be_falsey
        expect(@user2.has_role?(:owner, @object)).to be_falsey
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user2.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'is_member_of')).to exist
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user2.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'is_admin_of')).not_to exist
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user2.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'is_owner_of')).not_to exist
      end

      it "should update from member to owner of the #{described_class}" do
        put :update_member, params: { id: @object.id, user_id: @user2.id, previous_role: 'member', new_role: 'owner' }
        expect(response).to have_http_status :ok
        expect(@user2.has_role?(:admin, @object)).to be_truthy
        expect(@user2.has_role?(:owner, @object)).to be_truthy
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user2.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'is_admin_of')).to exist
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user2.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'is_owner_of')).to exist
      end

      it "should update from admin to owner of the #{described_class}" do
        @user2.add_role :admin, @object
        put :update_member, params: { id: @object.id, user_id: @user2.id, previous_role: 'admin', new_role: 'owner' }
        expect(response).to have_http_status :ok
        expect(@user2.has_role?(:owner, @object)).to be_truthy
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user2.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'is_owner_of')).to exist
      end

      it "should update from owner to admin of the #{described_class}" do
        @user2.add_role :admin, @object
        @user2.add_role :owner, @object
        put :update_member, params: { id: @object.id, user_id: @user2.id, previous_role: 'owner', new_role: 'admin' }
        expect(response).to have_http_status :ok
        expect(@user2.has_role?(:admin, @object)).to be_truthy
        expect(@user2.has_role?(:owner, @object)).to be_falsey
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user2.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'is_owner_of')).not_to exist
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user2.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'is_admin_of')).to exist
      end

      it 'should not allow less than one owner' do
        @object.users.each do |user|
          user.remove_role :owner, @object
        end
        @user.add_role :owner, @object
        put :update_member, params: { id: @object.id, user_id: @user.id, previous_role: 'owner', new_role: 'admin' }
        expect(response).to have_http_status :forbidden
        expect(@user.has_role?(:owner, @object)).to be_truthy
      end
    end

    describe '#remove_member' do
      before do
        @user.add_role :member, @object
        @user.add_role :admin, @object
        @user.add_role :owner, @object

        @user2 = create(:confirmed_user)
        @user2.add_role :member, @object
      end

      it "should remove a member of the #{described_class}" do
        delete :remove_member, params: { id: @object.id, user_id: @user2.id }
        expect(response).to have_http_status :found
        expect(@object.users.include?(@user2)).to be_falsey
        expect(@user2.has_role?(:member, @object)).to be_falsey
        expect(@user2.has_role?(:admin, @object)).to be_falsey
        expect(@user2.has_role?(:owner, @object)).to be_falsey
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user2.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'is_owner_of')).not_to exist
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user2.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'is_admin_of')).not_to exist
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user2.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'is_member_of')).not_to exist
      end

      it "should not remove a member of the #{described_class} if not owner or admin" do
        @user.remove_role :owner, @object
        @user.remove_role :admin, @object
        delete :remove_member, params: { id: @object.id, user_id: @user2.id }
        expect(response).to have_http_status :forbidden
        expect(@object.users.include?(@user2)).to be_truthy
        expect(@user2.has_role?(:member, @object)).to be_truthy
      end

      it "should not remove himself of the #{described_class} if last owner" do
        @object.users.each do |user|
          user.remove_role :owner, @object
        end
        @user.add_role :owner, @object
        delete :remove_member, params: { id: @object.id, user_id: @user.id }
        expect(response).to have_http_status :forbidden
        expect(@object.users.include?(@user)).to be_truthy
        expect(@user.has_role?(:owner, @object)).to be_truthy
      end
    end
  end

  # Testing the Auth methods

  context 'Not Signed In' do
    before(:each) do
      @user = create(:confirmed_user)
      @user2 = create(:confirmed_user)
      @user2.add_role :member, @object
    end

    it 'should require auth' do
      get :join, params: { id: @object.id }
      expect(response).to have_http_status :unauthorized
    end

    it 'should require auth' do
      put :leave, params: { id: @object.id }
      expect(response).to have_http_status :unauthorized
    end

    it 'should require auth' do
      put :update_member, params: { id: @object.id, user_id: @user2.id }
      expect(response).to have_http_status :unauthorized
    end

    it 'should require auth' do
      delete :remove_member, params: { id: @object.id, user_id: @user2.id }
      expect(response).to have_http_status :unauthorized
    end

    describe '#members should not require auth' do
      it "should get the members of the #{described_class}" do
        get :members_list, params: { id: @object.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response['members'].count).to eq 2
      end
    end

    describe '#has_membership should not require auth' do
      before do
        @user.add_role :member, @object
      end

      it 'should answer true' do
        get :has_membership, params: { id: @object.id, user_id: @user.id }
        expect(response).to have_http_status :found
      end

      it 'should answer false' do
        @user.remove_role :member, @object
        get :has_membership, params: { id: @object.id, user_id: @user.id }
        expect(response).to have_http_status :not_found
      end
    end
  end
end

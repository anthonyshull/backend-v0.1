# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'an object with reviewable' do |factory|
  before do
    @object = create(factory)
  end

  context 'Signed in' do
    before do
      @user = create(:confirmed_user)
      @user.add_role(:reviewer, @object)
      sign_in @user
    end

    describe '#review' do
      it "should review the #{described_class}" do

        put :review, params: { id: @object.id }
        expect(response).to have_http_status :ok
        expect(@user.reviewed?(@object)).to be true
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'has_reviewed')).to exist
      end
    end

    describe '#unreview' do
      before do
        @relation = @user.owned_relations.create(resource: @object, reviewed: true)
        @user.add_edge(@object, 'has_reviewed')
      end

      it "should unreview the #{described_class}" do
        expect(@user.reviewed?(@object)).to be true
        delete :review, params: { id: @object.id }
        expect(response).to have_http_status :ok
        expect(@user.reviewed?(@object)).to be false
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'has_reviewed')).not_to exist
      end
    end

    describe '#is_reviewed' do
      before do
        @relation = @user.owned_relations.create(resource: @object, reviewed: true)
      end

      it 'should answer true' do
        get :review, params: { id: @object.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response['has_reviewed']).to be true
      end

      it 'should answer false' do
        @relation.reviewed = false
        @relation.update(reviewed: false)
        get :review, params: { id: @object.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response['has_reviewed']).to be false
      end

      it 'should answer false' do
        @relation.delete
        get :review, params: { id: @object.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response['has_reviewed']).to eq false
      end
    end
  end

  # Testing the Auth methods

  context 'Not Signed In' do
    it 'should require auth' do
      put :review, params: { id: @object.id }
      expect(response).to have_http_status :unauthorized
    end

    it 'should require auth' do
      delete :review, params: { id: @object.id }
      expect(response).to have_http_status :unauthorized
    end

    it 'should require auth' do
      get :review, params: { id: @object.id }
      expect(response).to have_http_status :unauthorized
    end
  end
end

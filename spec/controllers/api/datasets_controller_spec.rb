# frozen_string_literal: true

require 'rails_helper'
require_relative 'shared_examples/recommendable'
require_relative 'shared_examples/clappable'
require_relative 'shared_examples/followable'
require_relative 'shared_examples/saveable'

RSpec.describe Api::DatasetsController, type: :controller do
  before do
    @user = create(:confirmed_user)
    sign_in @user
  end

  xdescribe 'Dataset' do
    it_behaves_like 'an object with recommendations', :dataset
  end

  xcontext 'User is logged in' do
    before do
      @user = create(:confirmed_user)
      sign_in @user
    end

    xdescribe 'and creates a' do
      context 'with valid params' do
        it 'Datasets::CkanDataset' do
          expect do
            post :create, params: { dataset: {
              type: 'Datasets::CkanDataset',
              url: 'https://data.gov.sg/dataset/rainfall-monthly-number-of-rain-days'
            } }
          end.to change(Datasets::CkanDataset, :count).by(1)
          json_response = JSON.parse(response.body)
          expect(response).to have_http_status(:created)
          expect(json_response['type']).to eq 'Datasets::CkanDataset'
          expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: 'Datasets::CkanDataset', targetable_node_id: json_response['id'], relation_type: 'is_author_of')).to exist
        end

        # it "Datasets::KapCodeDataset" do
        #   expect {
        #     post :create, params: { dataset: { type: "Datasets::KapCodeDataset", title: "test dataset" } }
        #   }.to change(Datasets::KapCodeDataset, :count).by(1)
        #   json_response = JSON.parse(response.body)
        #   expect(response.status).to eq 201
        #   expect(json_response["title"]).to eq 'test dataset'
        #   expect(json_response["type"]).to eq 'Datasets::KapCodeDataset'
        # end

        # it "Datasets::CustomDataset" do
        #   expect {
        #     post :create, params: { dataset: { type: "Datasets::CustomDataset", title: "test dataset" } }
        #   }.to change(Datasets::CustomDataset, :count).by(1)
        #   json_response = JSON.parse(response.body)
        #   expect(response.status).to eq 201
        #   expect(json_response["title"]).to eq 'test dataset'
        #   expect(json_response["type"]).to eq 'Datasets::CustomDataset'
        # end
      end

      context 'with invalid params' do
        it 'a Datasets::CkanDataset renders a JSON response with errors for the new dataset' do
          post :create, params: { dataset: { type: 'Datasets::CkanDataset', url: 'somethin' } }
          expect(response).to have_http_status(:unprocessable_entity)
          expect(response.content_type).to match('application/json')
        end
      end
    end

    xdescribe 'and builds' do
      context 'with valid params a' do
        it 'Datasets::CkanDataset' do
          expect do
            post :make, params: { dataset: {
              type: 'Datasets::CkanDataset',
              url: 'https://data.gov.sg/dataset/rainfall-monthly-number-of-rain-days'
            } }
          end.to change(Datasets::CkanDataset, :count).by(0)
          json_response = JSON.parse(response.body)
          expect(response).to have_http_status(:ok)
          expect(json_response['type']).to eq 'Datasets::CkanDataset'
          expect(json_response['short_title']).to eq 'rainfall-monthly-number-of-rain-days'
        end
      end

      context 'with invalid params a' do
        it 'Datasets::CkanDataset' do
          expect do
            post :make, params: { dataset: { type: 'Datasets::CkanDataset', url: 'somethin' } }
          end.to change(Datasets::CkanDataset, :count).by(0)
          expect(response).to have_http_status(:unprocessable_entity)
        end
      end
    end

    xdescribe 'and updates' do
      before(:each) do
        @dataset = Dataset.create(author_id: @user.id, type: 'Datasets::CkanDataset', url: 'https://data.gov.sg/dataset/rainfall-monthly-number-of-rain-days')
      end

      context 'with valid parameters a' do
        it 'Datasets::CkanDataset' do
          expect do
            patch :update, params: { id: @dataset.id, dataset: {
              type: 'Datasets::CkanDataset',
              url: 'https://data.gov.sg/dataset/resident-population-by-ethnicity-gender-and-age-group'
            } }
          end.to change(Datasets::CkanDataset, :count).by(0)
          json_response = JSON.parse(response.body)
          @dataset.reload
          expect(response.status).to eq 200
          expect(json_response['type']).to eq 'Datasets::CkanDataset'
          expect(@dataset.short_title).to eq 'resident-population-by-ethnicity-gender-and-age-group'
          expect(@dataset.data.count).to eq 3
          expect(@dataset.sources.count).to eq 2
          expect(@dataset.tags.count).to eq 10
        end
      end

      context 'with invalid parameters a' do
        it 'Datasets::CkanDataset' do
          expect do
            patch :update, params: { id: @dataset.id, dataset: {
              type: 'Datasets::CkanDataset', url: 'somethin'
            } }
          end.to change(Datasets::CkanDataset, :count).by(0)
          expect(response.status).to eq 422
          @dataset.reload
          expect(@dataset.short_title).to eq 'rainfall-monthly-number-of-rain-days'
          expect(@dataset.data.count).to eq 1
          expect(@dataset.sources.count).to eq 1
          expect(@dataset.tags.count).to eq 5
        end
      end
    end

    xdescribe 'and shows' do
      before(:each) do
        @dataset = Dataset.create(author_id: @user.id, type: 'Datasets::CkanDataset', url: 'https://data.gov.sg/dataset/rainfall-monthly-number-of-rain-days')
      end

      context 'with valid parameters a' do
        it 'Datasets::CkanDataset' do
          get :show, params: { id: @dataset.id }
          json_response = JSON.parse(response.body)
          @dataset.reload
          expect(response.status).to eq 200
          expect(json_response['type']).to eq 'Datasets::CkanDataset'
          expect(json_response['short_title']).to eq 'rainfall-monthly-number-of-rain-days'
          expect(json_response['data'].count).to eq 1
          expect(json_response['sources'].count).to eq 1
          expect(json_response['tags'].count).to eq 5
          expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: @dataset.class.name, targetable_node_id: @dataset.id, relation_type: 'has_visited')).to exist
        end
      end
    end

    xdescribe 'and destroy a' do
      before(:each) do
        @dataset = Dataset.create(author_id: @user.id, type: 'Datasets::CkanDataset', url: 'https://data.gov.sg/dataset/rainfall-monthly-number-of-rain-days')
      end

      it 'Datasets::CkanDataset' do
        expect do
          delete :destroy, params: { id: @dataset.id, dataset: {
            type: 'Datasets::CkanDataset',
            url: 'https://data.gov.sg/dataset/rainfall-monthly-number-of-rain-days'
          } }
        end.to change(Datasets::CkanDataset, :count).by(-1)
        expect(response.status).to eq 204
      end
    end
  end
end

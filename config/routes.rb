# frozen_string_literal: true

Rails.application.routes.draw do
  post '/api/graphql', to: 'graphql#execute'
  resources :external_links
  # devise_for :admins
  # mount RailsAdmin::Engine => '/jogladmin', as: 'rails_admin'

  mount Sidekiq::Web => '/sidekiq'
  require 'sidekiq/cron/web'

  root 'api/pages#index'
  mount_devise_token_auth_for 'User', at: 'api/auth'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    root 'pages#index'

    #####################################################################################################
    ######## Definitions of the concerns for actions that are shared between different objects ##########
    #####################################################################################################

    # short_title check
    # resources :challenges, :communities, :programs, :projects, :spaces do
    concern :short_titleable do
      collection do
        get  '/exists/:short_title'         => :short_title_exist
        get  '/getid/:short_title'          => :get_id_from_short_title
      end
    end

    # Upload Mechanics
    # resources :needs, :projects, :challenges, :posts, :spaces do
    concern :documentable do
      collection do
        post    '/:id/documents'               => :upload_document
        delete  '/:id/documents/:document_id'  => :remove_document
      end
    end

    # Follow Mechanics
    # resources :users, :projects, :communities, :challenges, :programs, :needs do
    concern :followable do
      collection do
        put    '/:id/follow'           => :follow
        get    '/:id/follow'           => :follow
        delete '/:id/follow' => :follow
        get    '/:id/followers' => :followers
      end
    end

    # Hooks Mechanics
    # resources :projects, :challenges do
    concern :hookable do
      collection do
        post   '/:id/hooks'                     => :create_external_hook
        patch  '/:id/hooks/:hook_id'            => :update_external_hook
        delete '/:id/hooks/:hook_id'            => :delete_external_hook
        get    '/:id/hooks'                     => :get_external_hooks
      end
    end

    # Customfield and data Mechanics
    concern :customdatable do
      collection do
        post   '/:id/customfield'                     => :create_custom_field
        patch  '/:id/customfield/:field_id'           => :update_custom_field
        delete '/:id/customfield/:field_id'           => :delete_custom_field
        get    '/:id/customfield'                     => :get_custom_fields
        post   '/:id/customdata/:field_id'            => :create_custom_data
        patch  '/:id/customdata/:field_id'            => :update_custom_data
        get    '/:id/customdata/:field_id'            => :get_custom_data
        get    '/:id/customdata'                      => :get_my_custom_datas
      end
    end

    # Clap Mechanics
    # resources :users, :projects, :communities, :challenges, :programs, :posts, :needs do
    concern :clapable do
      collection do
        put    '/:id/clap'             => :clap
        get    '/:id/clap'             => :clap
        delete '/:id/clap'             => :clap
        get    '/:id/clappers'         => :clappers
      end
    end

    # Save Mechanics
    # resources :users, :projects, :communities, :challenges, :needs, :programs do
    concern :saveable do
      collection do
        put    '/:id/save'             => :save
        get    '/:id/save'             => :save
        delete '/:id/save'             => :save
      end
    end

    # Join and Leave Mechanics
    # resources :projects, :communities, :challenges, :needs do
    concern :joinable do
      collection do
        put   '/:id/join'           => :join
        put   '/:id/leave'          => :leave
      end
    end

    # Members Mechanics
    # resources :projects, :communities, :challenges, :programs, :needs do
    concern :memberable do
      collection do
        post '/:id/invite' => :invite
        post '/:id/is_member' => :has_membership
        get '/:id/members' => :members_list
        post '/:id/members' => :update_member
        delete '/:id/members/:user_id' => :remove_member
      end
    end

    # Members Mechanics
    # resources :projects, :communities, :challenges, :programs, :needs do
    concern :reviewable do
      collection do
        put '/:id/review'     => :review
        get '/:id/review'     => :review
        delete '/:id/review'  => :review
      end
    end

    # Banner
    # resources :projects, :communities, :challenges, :programs do
    concern :bannerable do
      collection do
        post '/:id/banner' => :upload_banner
        delete '/:id/banner' => :remove_banner
      end
    end

    # Avatar
    # resources :users do
    concern :avatarable do
      collection do
        post '/:id/avatar' => :upload_avatar
        delete '/:id/avatar' => :remove_avatar
      end
    end

    # Recommendations
    concern :recommendable do
      collection do
        get '/recommended' => :recommended
        get '/:id/similar' => :similar
      end
    end

    # FAQ
    concern :faqable do
      collection do
        post '/:id/faq' => :faq_create
        get '/:id/faq' => :faq_index
        patch '/:id/faq/:document_id' => :faq_update
        get '/:id/faq/:document_id' => :faq_show
        delete '/:id/faq/:document_id' => :faq_destroy
      end
    end

    # External Links
    concern :linkable do
      collection do
        post '/:id/links' => :create_link
        get '/:id/links' => :index_link
        patch '/:id/links/:link_id' => :update_link
        delete '/:id/links/:link_id' => :destroy_link
      end
    end

    # Resources items
    concern :resourceable do
      collection do
        get      '/:id/resources'                     => :index_resource
        post     '/:id/resources'                     => :add_resource
        patch    '/:id/resources/:resource_id'        => :update_resource
        delete   '/:id/resources/:resource_id'        => :remove_resource
      end
    end

    #####################################################################################################
    ########                Definitions of the main ressources routing and CRUD                ##########
    #####################################################################################################

    # Datasets specific routes
    resources :datasets, concerns: %i[recommendable clapable followable saveable] do
      collection do
        post   '/make' => :make
      end
    end

    # Users Specific routes

    resources :users, except: [:destroy], concerns: %i[
      avatarable
      clapable
      followable
      linkable
      recommendable
      saveable
    ] do
      collection do
        delete '/'                         => :destroy
        get    '/exists/:nickname'         => :nickname_exist
        post   '/resend_confirmation'      => :resend_confirmation
        get    '/saved_objects'            => :saved_objects
        get    '/:id/mutual'               => :mutual
        get    '/:id/following'            => :following
        get    '/:id/objects/:object_type' => :user_object
        post   '/:id/send_email'           => :send_private_email
      end
    end

    # Project Specific routes
    resources :projects, concerns: %i[bannerable clapable followable saveable
                                      short_titleable documentable hookable linkable
                                      joinable memberable customdatable recommendable reviewable] do
      collection do
        get '/mine'                      => :my_projects
        get '/:id/needs'                 => :index_needs
      end
    end

    # Needs Specific routes
    resources :needs, concerns: %i[joinable memberable saveable
                                   clapable followable documentable
                                   recommendable] do
      collection do
        get '/mine' => :my_needs
      end
    end

    # communities Specific routes
    resources :communities, concerns: %i[bannerable clapable followable saveable
                                         short_titleable documentable hookable
                                         joinable memberable recommendable linkable] do
      collection do
        get '/mine' => :my_communities
      end
    end

    # Posts Specific routes
    resources :posts, except: :index, concerns: %i[documentable saveable clapable recommendable] do
      collection do
        post   '/:id/comment'                => :comment
        patch  '/:id/comment/:comment_id'    => :comment
        delete '/:id/comment/:comment_id' => :comment
      end
    end

    resources :feeds, except: %i[create update destroy] do
      collection do
        get      '/all'                      => :indexall
        delete   '/:id/:post_id'             => :remove_post
      end
    end

    # Challenge Specific routes
    resources :challenges, concerns: %i[
      avatarable
      bannerable
      clapable
      documentable
      faqable
      followable
      hookable
      joinable
      linkable
      memberable
      recommendable
      saveable
      short_titleable
    ] do
      collection do
        get   '/:id/projects'               => :index_projects
        get   '/:id/needs'                  => :index_needs
        post '/:id/projects/:project_id'  => :set_project_status
        put '/:id/projects/:project_id'   => :attach
        delete '/:id/projects/:project_id'  => :remove
        get '/mine'                         => :my_challenges
        get '/can_create'                   => :can_create
      end
    end

    # Program Specific routes
    resources :programs, concerns: %i[bannerable avatarable clapable followable saveable
                                      short_titleable documentable hookable linkable
                                      joinable memberable recommendable faqable resourceable] do
      resources :boards do
        collection do
          post    '/:id/users/:user_id'         => :add_user
          delete  '/:id/users/:user_id'         => :remove_user
        end
      end

      collection do
        get    '/:id/needs'                      => :index_needs
        get    '/:id/projects'                   => :index_projects
        get    '/:id/challenges'                 => :index_challenges
        put    '/:id/challenges/:challenge_id'   => :attach
        delete '/:id/challenges/:challenge_id'   => :remove
        get    '/mine'                           => :my_programs
        get    '/can_create'                     => :can_create
      end
    end

    # Space Specific routes
    resources :affiliations, only: %i[create update destroy]

    resources :spaces, concerns: %i[bannerable avatarable clapable followable saveable
                                    short_titleable documentable hookable linkable
                                    joinable memberable recommendable faqable resourceable] do
      resources :boards do
        collection do
          post    '/:id/users/:user_id'         => :add_user
          delete  '/:id/users/:user_id'         => :remove_user
        end
      end

      member do
        get 'affiliates'
      end

      collection do
        get    '/:id/needs'                      => :index_needs
        get    '/:id/projects'                   => :index_projects
        get    '/:id/challenges'                 => :index_challenges
        get    '/:id/programs'                   => :index_programs
        put    '/:id/programs/:program_id'       => :attach
        delete '/:id/programs/:program_id'       => :remove
        get    '/mine'                           => :my_spaces
        get    '/can_create'                     => :can_create
      end
    end

    resources :boards, except: %i[index show create update destroy] do
      collection do
        post    '/:id/users/:user_id'         => :add_user
        delete  '/:id/users/:user_id'         => :remove_user
      end
    end

    resources :networks, only: %i[index show]

    resources :notifications, only: %i[index show] do
      collection do
        put   '/:id/read'                        => :read
        put   '/:id/unread'                      => :unread
        put   '/readall'                         => :all_read
        put   '/unreadall'                       => :all_unread
        get   '/settings'                        => :settings
        post  '/settings'                        => :set_settings
      end
    end

    resources :algolium, only: [:create], defaults: { format: :json } do
      collection do
      end
    end

    resources :skills, only: [:index]

    # Challenge Specific routes
  end
end

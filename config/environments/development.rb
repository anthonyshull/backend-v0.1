# frozen_string_literal: true

require 'active_support/core_ext/integer/time'

Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  config.serve_static_assets = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports.
  config.consider_all_requests_local = true

  # Enable/disable caching. By default caching is disabled.
  # Run rails dev:cache to toggle caching.
  if Rails.root.join('tmp', 'caching-dev.txt').exist?
    config.action_controller.perform_caching = true

    config.cache_store = :memory_store
    config.public_file_server.headers = {
      'Cache-Control' => "public, max-age=#{2.days.to_i}"
    }
  else
    config.action_controller.perform_caching = false

    config.cache_store = :null_store
  end

  # Store uploaded files on the local file system (see config/storage.yml for options).
  config.active_storage.service = :local

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.perform_caching = false
  config.action_mailer.show_previews = true

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise exceptions for disallowed deprecations.
  config.active_support.disallowed_deprecation = :raise
  # Tell Active Support which deprecation messages to disallow.
  config.active_support.disallowed_deprecation_warnings = []

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Highlight code that triggered database queries in logs.
  config.active_record.verbose_query_logs = true

  config.reload_classes_only_on_change = false

  # Raises error for missing translations.
  # config.i18n.raise_on_missing_translations = true

  # Annotate rendered view with file names.
  # config.action_view.annotate_rendered_view_with_filenames = true

  # Use an evented file watcher to asynchronously detect changes in source code,
  # routes, locales, etc. This feature depends on the listen gem.
  config.file_watcher = ActiveSupport::EventedFileUpdateChecker

  # Uncomment if you wish to allow Action Cable access from any origin.
  # config.action_cable.disable_request_forgery_protection = true

  Rails.application.routes.default_url_options[:host] = ENV['BACKEND_URL']
  config.action_mailer.default_url_options = { host: 'localhost:3001' }

  config.action_mailer.default_options = { from: 'dev@jogl.io' }
  # THIS NEEDS TO BE ACTIVATED ONLY TO TEST POSTMARK AND REMOVED OTHERWISE
  # config.action_mailer.delivery_method = :postmark

  config.action_mailer.smtp_settings = { address: 'localhost', port: 1025, openssl_verify_mode: 'none' }

  # Below is the config for logstasher

  if ENV['ELASTICSEARCH_HOST'].present? && ENV['ELASTICSEARCH_USER'].present? && ENV['ELASTICSEARCH_PASSWORD'].present?
    config.logstasher.enabled = true

    config.logstasher.source = 'dev_env'

    config.log_level = :debug
    config.autoflush_log = true

    config.logstasher.logger = LogStashLogger.new(
      type: :multi_delegator,
      outputs: [
        # { uri: ENV["LOGSTASH_URI"], sync: true},
        { type: :udp, host: '127.0.0.1', port: 5044, sync: true },
        # { type: :udp, host: ENV["LOGSTASH_SERVER"], port: ENV["LOGSTASH_PORT"]},
        { type: :stdout, formatter: ->(_severity, _time, progname, msg) { "[#{progname}] #{msg}" } }
      ]
    )
  end
  # ActiveRecord::Base.logger = Logger.new( Rails.root.join("log", "development.log") )
  config.active_record.logger = Logger.new(Rails.root.join('log', 'development.log'))

  config.after_initialize do
    Bullet.enable = true
    Bullet.bullet_logger = true
    Bullet.rails_logger = true
  end
end

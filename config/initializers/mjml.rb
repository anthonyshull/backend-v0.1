Mjml.setup do |config|
  # :erb (default), :slim, :haml, or any other you are using
  config.template_language = :erb

  # set to `false` to ignore errors silently
  config.raise_render_exception = true

  # OPTIMIZE: the size of your email
  config.beautify = true
  config.minify = false
  config.validation_level = 'strict'
end
